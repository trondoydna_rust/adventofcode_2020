fn main() {
    task1();
    task2();
}

/// 1920
fn task1() {
    let mut input: Vec<u32> = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .map(|line| line.parse().unwrap())
        .collect();
    input.sort();

    let (ones, threes) = count_jolts_diffs(input);

    println!("Task 1 answer: {:#?}", ones * threes);
}

/// < 562949953421312
fn task2() {
    let mut input: Vec<u32> = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .map(|line| line.parse().unwrap())
        .collect();
    input.sort();

    let combinations = combinations(input);

    println!("Task 2 answer: {}", combinations);
}

fn count_jolts_diffs(input: Vec<u32>) -> (u32, u32) {
    let (_, ones, threes) = input
        .iter()
        .fold((0, 0, 1), |(prev_jolt, mut ones, mut threes), &next| {
            match next - prev_jolt {
                1 => ones += 1,
                3 => threes += 1,
                _ => (),
            };
            (next, ones, threes)
        });

    (ones, threes)
}

fn combinations(input: Vec<u32>) -> u64 {
    let (groups, _) = input.iter().fold(
        (vec![], 0),
        |(mut groups, prev): (Vec<Vec<u32>>, u32), &next| {
            if prev + 1 == next {
                if let Some(group) = groups.last_mut() {
                    group.push(next);
                } else {
                    groups.push(vec![next])
                }
            } else {
                groups.push(vec![next]);
            }
            (groups, next)
        },
    );

    groups.iter().fold(
        1,
        |sum, next| {
            match next.len() {
                1 | 2 => sum,
                3 => sum * 2,
                4 => sum * 4,
                5 => sum * 6,
                6 => sum * 10,
                n => panic!(n),
            }
        }
    )
}

/*
4 group + 3 group = 8

// all
4,5,6,7,10,11,12

// first group
4, ,6,7,10,11,12
4,5, ,7,10,11,12
4, , ,7,10,11,12

// second group
4,5,6,7,10,  ,12

// both groups
4, ,6,7,10,  ,12
4,5, ,7,10,  ,12

// none
4, , ,7,10,  ,12
*/

/*
4 group + 3 group + 3 group = 16

// all
4,5,6,7,10,11,12,15,16,17

// first group
4, ,6,7,10,11,12,15,16,17
4,5, ,7,10,11,12,15,16,17
4, , ,7,10,11,12,15,16,17

// second group
4,5,6,7,10,  ,12,15,16,17

// third group
4,5,6,7,10,11,12,15,  ,17

// first and second
4, ,6,7,10,  ,12,15,16,17
4,5, ,7,10,  ,12,15,16,17
4, , ,7,10,  ,12,15,16,17

// first and third
4, ,6,7,10,11,12,15,  ,17
4,5, ,7,10,11,12,15,  ,17
4, , ,7,10,11,12,15,  ,17

// second and third
4,5,6,7,10,  ,12,15,  ,17

// none
4,5, ,7,10,  ,12,15,  ,17
4, ,6,7,10,  ,12,15,  ,17
4, , ,7,10,  ,12,15,  ,17
*/

/*
4 group + 4 group = 16

// all
4,5,6,7,10,11,12,13

// first group
4, ,6,7,10,11,12,13
4,5, ,7,10,11,12,13
4, , ,7,10,11,12,13

// second group
4,5,6,7,10,  ,12,13
4,5,6,7,10,11,  ,13
4,5,6,7,10,  ,  ,13

// both
4, ,6,7,10,  ,12,13
4,5, ,7,10,  ,12,13
4, , ,7,10,  ,12,13
4, ,6,7,10,11,  ,13
4,5, ,7,10,11,  ,13
4, , ,7,10,11,  ,13

// none
4,5, ,7,10,  ,  ,13
4, ,6,7,10,  ,  ,13
4, , ,7,10,  ,  ,13
*/

/*
5 group = 5

1, ,3,4,5
1, , ,4,5
1, ,3, ,5
1,2, ,4,5
1,2, , ,5
*/

/*
6 group = 11

1,2,3,4,5,6
1, ,3,4,5,6
1, , ,4,5,6
1, ,3, ,5,6
1, ,3,4, ,6
1, , ,4, ,6
1, ,3, , ,6
1,2, ,4,5,6
1,2, , ,5,6
1,2, ,4, ,6
1,2,3, , ,6
*/

#[cfg(test)]
mod tests {
    use crate::{ count_jolts_diffs, combinations};

    const EXAMPLE1: &str = r#"
16
10
15
5
1
11
7
19
6
12
4
    "#;

    const EXAMPLE2: &str = r#"
28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3
    "#;

    const MY_EXAMPLE1: &str = r#"
4
5
6
7
10
11
12
    "#;

    const MY_EXAMPLE2: &str = r#"
4
5
6
7
10
11
12
15
16
17
        "#;

    const MY_EXAMPLE3: &str = r#"
4
5
6
7
10
11
12
13
    "#;

    const MY_EXAMPLE4: &str = r#"
1
2
3
4
5
6
    "#;

    #[test]
    fn task1_example1() {
        let mut input: Vec<u32> = EXAMPLE1
            .trim()
            .lines()
            .map(|line| line.parse().unwrap())
            .collect();
        input.sort();

        let (ones, threes) = count_jolts_diffs(input);

        assert_eq!(7, ones);
        assert_eq!(5, threes);
        assert_eq!(7 * 5, ones * threes)
    }

    #[test]
    fn task1_example2() {
        let mut input: Vec<u32> = EXAMPLE2
            .trim()
            .lines()
            .map(|line| line.parse().unwrap())
            .collect();
        input.sort();

        let (ones, threes) = count_jolts_diffs(input);

        assert_eq!(22, ones);
        assert_eq!(10, threes);
        assert_eq!(22 * 10, ones * threes)
    }

    #[test]
    fn task2_example1() {
        let mut input: Vec<u32> = EXAMPLE1
            .trim()
            .lines()
            .map(|line| line.parse().unwrap())
            .collect();
        input.sort();

        let combinations = combinations(input);

        assert_eq!(8, combinations);
    }

    #[test]
    fn task2_example2() {
        let mut input: Vec<u32> = EXAMPLE2
            .trim()
            .lines()
            .map(|line| line.parse().unwrap())
            .collect();
        input.sort();

        let combinations = combinations(input);

        assert_eq!(19208, combinations);
    }

    #[test]
    fn task2_my_example1() {
        let mut input: Vec<u32> = MY_EXAMPLE1
            .trim()
            .lines()
            .map(|line| line.parse().unwrap())
            .collect();
        input.sort();

        let combinations = combinations(input);

        assert_eq!(8, combinations);
    }

    #[test]
    fn task2_my_example2() {
        let mut input: Vec<u32> = MY_EXAMPLE2
            .trim()
            .lines()
            .map(|line| line.parse().unwrap())
            .collect();
        input.sort();

        let combinations = combinations(input);

        assert_eq!(16, combinations);
    }

    #[test]
    fn task2_my_example3() {
        let mut input: Vec<u32> = MY_EXAMPLE3
            .trim()
            .lines()
            .map(|line| line.parse().unwrap())
            .collect();
        input.sort();

        let combinations = combinations(input);

        assert_eq!(16, combinations);
    }

    #[test]
    fn task2_my_example4() {
        let mut input: Vec<u32> = MY_EXAMPLE4
            .trim()
            .lines()
            .map(|line| line.parse().unwrap())
            .collect();
        input.sort();

        let combinations = combinations(input);

        assert_eq!(11, combinations);
    }
}
