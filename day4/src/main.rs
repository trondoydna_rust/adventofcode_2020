use std::str::FromStr;

fn main() {
    task1();
    task2();
}

/// 190
fn task1() {
    let input: VecPassports = std::fs::read_to_string("input").unwrap().parse().unwrap();
    println!(
        "Task 1 answer: {}",
        input
            .0
            .iter()
            .filter(|passport| passport.is_valid_task1())
            .count()
    );
}

/// 121
fn task2() {
    let input: VecPassports = std::fs::read_to_string("input").unwrap().parse().unwrap();
    println!(
        "Task 2 answer: {}",
        input
            .0
            .iter()
            .filter(|passport| passport.is_valid_task1() && passport.is_valid_task2())
            .count()
    );
}

struct VecPassports(Vec<Passport>);

#[derive(Debug)]
struct Passport {
    fields: Vec<PassportField>,
}

#[derive(Debug)]
enum PassportField {
    BYR(String),
    IYR(String),
    EYR(String),
    HGT(String),
    HCL(String),
    ECL(String),
    PID(String),
    CID(String),
}

impl Passport {
    fn is_valid_task1(&self) -> bool {
        self.fields
            .iter()
            .any(|field| matches!(field, PassportField::BYR(_)))
            && self
                .fields
                .iter()
                .any(|field| matches!(field, PassportField::IYR(_)))
            && self
                .fields
                .iter()
                .any(|field| matches!(field, PassportField::EYR(_)))
            && self
                .fields
                .iter()
                .any(|field| matches!(field, PassportField::HGT(_)))
            && self
                .fields
                .iter()
                .any(|field| matches!(field, PassportField::HCL(_)))
            && self
                .fields
                .iter()
                .any(|field| matches!(field, PassportField::ECL(_)))
            && self
                .fields
                .iter()
                .any(|field| matches!(field, PassportField::PID(_)))
    }

    fn is_valid_task2(&self) -> bool {
        self.fields.iter().all(|field| field.is_valid_task2())
    }
}

impl PassportField {
    fn is_valid_task2(&self) -> bool {
        match self.try_is_valid_task2() {
            Ok(b) => b,
            Err(_) => false,
        }
    }

    fn try_is_valid_task2(&self) -> Result<bool, Box<dyn std::error::Error>> {
        Ok(match self {
            PassportField::BYR(byr) => {
                let byr: u16 = byr.parse()?;
                byr >= 1920 && byr <= 2002
            }
            PassportField::IYR(iyr) => {
                let iyr: u16 = iyr.parse()?;
                iyr >= 2010 && iyr <= 2020
            }
            PassportField::EYR(eyr) => {
                let eyr: u16 = eyr.parse()?;
                eyr >= 2020 && eyr <= 2030
            }
            PassportField::HGT(hgt) => {
                let num: String = hgt.chars().take_while(|c| c.is_numeric()).collect();
                let num: u32 = num.parse()?;
                let unit: String = hgt.chars().skip_while(|c| c.is_numeric()).collect();

                match unit.as_str() {
                    "cm" => num >= 150 && num <= 193,
                    "in" => num >= 59 && num <= 76,
                    _ => false,
                }
            }
            PassportField::HCL(hcl) => {
                let mut chars = hcl.chars();
                if let Some('#') = chars.next() {
                    chars.filter(|c| matches!(c, '0'..='9' | 'a'..='f')).count() == 6
                } else {
                    false
                }
            }
            PassportField::ECL(ecl) => match ecl.as_str() {
                "amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => true,
                _ => false,
            },
            PassportField::PID(pid) => pid.chars().filter(|c| c.is_numeric()).count() == 9,
            PassportField::CID(_) => true,
        })
    }
}

impl FromStr for VecPassports {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let passports: Result<Vec<Passport>, Self::Err> = s
            .split("\n\n")
            .map(|passport| passport.parse::<Passport>())
            .collect();

        Ok(VecPassports(passports?))
    }
}

impl FromStr for Passport {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let fields: Result<Vec<PassportField>, Self::Err> = s
            .split_whitespace()
            .map(|field| field.parse::<PassportField>())
            .collect();

        Ok(Passport { fields: fields? })
    }
}

impl FromStr for PassportField {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut fields = s.split(':');
        let field_name = fields
            .next()
            .ok_or(format!("Missing field name in {}", s))?;
        let field_val = fields
            .next()
            .ok_or(format!("Missing field value in {}", s))?
            .to_owned();

        Ok(match field_name {
            "byr" => PassportField::BYR(field_val),
            "iyr" => PassportField::IYR(field_val),
            "eyr" => PassportField::EYR(field_val),
            "hgt" => PassportField::HGT(field_val),
            "hcl" => PassportField::HCL(field_val),
            "ecl" => PassportField::ECL(field_val),
            "pid" => PassportField::PID(field_val),
            "cid" => PassportField::CID(field_val),
            _ => panic!("Unknown field name {} in {}", field_name, s),
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::VecPassports;

    const INPUT_TASK1: &str = r#"
ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in
    "#;

    const INPUT_TASK2_INVALID: &str = r#"
eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007
    "#;

    const INPUT_TASK2_VALID: &str = r#"
pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
    "#;

    #[test]
    fn test_task1() {
        let passports: VecPassports = INPUT_TASK1.trim().parse().unwrap();
        let passports = passports.0;

        assert_eq!(
            2,
            passports
                .iter()
                .filter(|passport| passport.is_valid_task1())
                .count()
        );
    }

    #[test]
    fn test_task2_invalid() {
        let passports: VecPassports = INPUT_TASK2_INVALID.trim().parse().unwrap();
        let passports = passports.0;
        let count = passports
            .iter()
            .filter(|passport| {
                let valid = passport.is_valid_task1() && passport.is_valid_task2();
                if valid {
                    eprintln!("Should not be valid {:#?}", passport);
                }
                valid
            })
            .count();

        assert_eq!(0, count);
    }

    #[test]
    fn test_task2_valid() {
        let passports: VecPassports = INPUT_TASK2_VALID.trim().parse().unwrap();
        let passports = passports.0;
        let count = passports
            .iter()
            .filter(|passport| {
                let valid = passport.is_valid_task1() && passport.is_valid_task2();
                if !valid {
                    eprintln!("Should be valid {:#?}", passport);
                }
                valid
            })
            .count();

        assert_eq!(4, count);
    }
}
