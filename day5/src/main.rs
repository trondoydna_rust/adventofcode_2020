use std::{str::FromStr, collections::HashSet};

fn main() {
    task1();
    task2();
}

/// 874
fn task1() {
    let directions: Result<Vec<Directions>, Box<dyn std::error::Error>> = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .map(|line| line.parse())
        .collect();
    let directions = directions.unwrap();

    println!("Task 1 answer: {}", directions.iter().map(|d| d.calc_seat_id()).max().unwrap());
}

/// 594
fn task2() {
    let directions: Result<Vec<Directions>, Box<dyn std::error::Error>> = std::fs::read_to_string("input")
        .unwrap()
        .lines()
        .map(|line| line.parse())
        .collect();
    let directions = directions.unwrap();

    let taken_seat_ids: HashSet<usize> = directions.iter().map(|d| d.calc_seat_id()).collect();
    let available_seat_ids = (1..)
        .filter(|i| !taken_seat_ids.contains(i))
        .find(|i| taken_seat_ids.contains(&(i - 1)) && taken_seat_ids.contains(&(i + 1)));

    println!("Task 2 answer: {}", available_seat_ids.unwrap());
}

#[derive(Debug)]
enum RowDirection {
    Front,
    Back,
}

#[derive(Debug)]
enum ColumnDirection {
    Left,
    Right,
}

#[derive(Debug)]
struct Directions {
    rows: [RowDirection; 7],
    columns: [ColumnDirection; 3],
}

impl Directions {
    fn calc_seat_id(&self) -> usize {
        let row_id = self.rows
            .iter()
            .fold(
                (0, 127),
                |(start, end): (usize, usize), direction| {
                    let mid = start + ((end - start) / 2);
                    match direction {
                        RowDirection::Front => (start, mid),
                        RowDirection::Back => ((mid + 1), end),
                    }
                }
            )
            .0;
        
        let column_id = self.columns
            .iter()
            .fold(
                (0, 7),
                |(start, end): (usize, usize), direction| {
                    let mid = start + ((end - start) / 2);
                    match direction {
                        ColumnDirection::Left => (start, mid),
                        ColumnDirection::Right => ((mid + 1), end),
                    }
                }
            )
            .0;

        row_id * 8 + column_id
    }
}

impl FromStr for Directions {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut chars = s.chars();
        let rows = [
            chars.next().ok_or(s)?.into(),
            chars.next().ok_or(s)?.into(),
            chars.next().ok_or(s)?.into(),
            chars.next().ok_or(s)?.into(),
            chars.next().ok_or(s)?.into(),
            chars.next().ok_or(s)?.into(),
            chars.next().ok_or(s)?.into(),
        ];
        let columns = [
            chars.next().ok_or(s)?.into(),
            chars.next().ok_or(s)?.into(),
            chars.next().ok_or(s)?.into(),
        ];
        Ok(Directions { rows, columns })
    }
}

impl From<char> for RowDirection {
    fn from(c: char) -> Self {
        match c {
            'F' => RowDirection::Front,
            'B' => RowDirection::Back,
            _ => panic!("Unknown row direction {}", c),
        }
    }
}

impl From<char> for ColumnDirection {
    fn from(c: char) -> Self {
        match c {
            'L' => ColumnDirection::Left,
            'R' => ColumnDirection::Right,
            _ => panic!("Unknown column direction {}", c),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::Directions;

    #[test]
    fn test_task1() {
        let directions: Directions = "BFFFBBFRRR".parse().unwrap();
        assert_eq!(567, directions.calc_seat_id());

        let directions: Directions = "FFFBBBFRRR".parse().unwrap();
        assert_eq!(119, directions.calc_seat_id());


        let directions: Directions = "BBFFBBFRLL".parse().unwrap();
        assert_eq!(820, directions.calc_seat_id());
    }
}
