use std::collections::HashSet;
use std::str::FromStr;

fn main() {
    task1();
    task2();
}

/// 185
fn task1() {
    let input: VecBagRules = std::fs::read_to_string("input").unwrap().parse().unwrap();
    let bags = input.find_distinct_bags_with_content("shiny gold");

    println!("Task 1 answer: {:#?}", bags.len());
}

/// 89084
fn task2() {
    let input: VecBagRules = std::fs::read_to_string("input").unwrap().parse().unwrap();
    let count = input.count_total_bags_needed("shiny gold");
    let count_except_self = count - 1;

    println!("Task 2 answer: {}", count_except_self);
}

#[derive(Debug)]
struct VecBagRules(Vec<BagRule>);

#[derive(Debug, PartialEq, Eq, Hash)]
struct BagRule {
    name: String,
    contents: Vec<BagContent>,
}

#[derive(Debug, PartialEq, Eq, Hash)]
struct BagContent {
    name: String,
    count: u32,
}

impl VecBagRules {
    fn find_distinct_bags_with_content(&self, name: &str) -> HashSet<&BagRule> {
        self.0
            .iter()
            .filter(|bag| {
                bag.contents
                    .iter()
                    .any(|content| content.name.as_str() == name)
            })
            .flat_map(|bag| {
                let mut bags = self.find_distinct_bags_with_content(bag.name.as_str());
                bags.insert(bag);
                bags
            })
            .collect()
    }

    fn count_total_bags_needed(&self, name: &str) -> u32 {
        let bag = self.0.iter().find(|bag| bag.name == name).unwrap();

        let child_count: u32 = bag
            .contents
            .iter()
            .map(|content| content.count * self.count_total_bags_needed(content.name.as_str()))
            .sum();

        child_count + 1 // + self
    }
}

impl FromStr for VecBagRules {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let r: Result<Vec<BagRule>, Self::Err> = s.lines().map(|line| line.parse()).collect();
        Ok(VecBagRules(r?))
    }
}

impl FromStr for BagRule {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(" bags contain ");

        let name = split.next().unwrap().to_owned();
        let contents = split.next().unwrap();
        let contents = if contents == "no other bags." {
            vec![]
        } else {
            let r: Result<Vec<BagContent>, Self::Err> =
                contents.split(", ").map(|line| line.parse()).collect();
            r?
        };

        Ok(BagRule { name, contents })
    }
}

impl FromStr for BagContent {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let count = s.get(0..1).unwrap().parse()?;
        let name = s
            .get(2..)
            .unwrap()
            .trim_end_matches(".")
            .trim_end_matches("s")
            .trim_end_matches(" bag")
            .to_owned();

        Ok(BagContent { name, count })
    }
}

#[cfg(test)]
mod tests {}
