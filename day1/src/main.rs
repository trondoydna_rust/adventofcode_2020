fn main() {
    task1();
    task2();
}

/// 989824
fn task1() {
    let input: Vec<u32> = std::fs::read_to_string("input")
        .unwrap()
        .split_whitespace()
        .map(|line| line.parse::<u32>().unwrap())
        .collect();

    let two_numbers = find_two_numbers_that_sum_to_2020(input).unwrap();
    let sum = two_numbers.0 * two_numbers.1;

    println!("Task 1 answer: {}", sum);
}

fn find_two_numbers_that_sum_to_2020(numbers: Vec<u32>) -> Option<(u32, u32)> {
    numbers
        .iter()
        .flat_map(|&n1| {
            numbers
                .iter()
                .map(|&n2| (n1, n2))
                .collect::<Vec<(u32, u32)>>()
        })
        .find(|(n1, n2)| n1 + n2 == 2020)
}

/// 66432240
fn task2() {
    let input: Vec<u32> = std::fs::read_to_string("input")
        .unwrap()
        .split_whitespace()
        .map(|line| line.parse::<u32>().unwrap())
        .collect();

    let three_numbers = find_three_numbers_that_sum_to_2020(input).unwrap();
    let sum = three_numbers.0 * three_numbers.1 * three_numbers.2;

    println!("Task 2 answer: {}", sum);
}

fn find_three_numbers_that_sum_to_2020(numbers: Vec<u32>) -> Option<(u32, u32, u32)> {
    numbers
        .iter()
        .flat_map(|&n1| {
            numbers
                .iter()
                .flat_map(|&n2| {
                    numbers
                        .iter()
                        .map(|&n3| (n1, n2, n3))
                        .collect::<Vec<(u32, u32, u32)>>()
                })
                .collect::<Vec<(u32, u32, u32)>>()
        })
        .find(|(n1, n2, n3)| n1 + n2 + n3 == 2020)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_task1() {
        let numbers = vec![1721, 979, 366, 299, 675, 1456];

        let two_numbers = find_two_numbers_that_sum_to_2020(numbers).unwrap();

        assert_eq!(two_numbers, (1721, 299));
    }

    #[test]
    fn test_task2() {
        let numbers = vec![1721, 979, 366, 299, 675, 1456];

        let two_numbers = find_three_numbers_that_sum_to_2020(numbers).unwrap();

        assert_eq!(two_numbers, (979, 366, 675));
    }
}
