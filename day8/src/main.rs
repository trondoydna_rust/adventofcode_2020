use std::collections::HashSet;
use std::str::FromStr;

fn main() {
    task1();
    task2();
}

/// 1528
fn task1() {
    let instruction_set: InstructionSet =
        std::fs::read_to_string("input").unwrap().parse().unwrap();
    let accumulator = Computer::execute_instructions_until_repeated(instruction_set);

    println!("Task 1 answer: {:#?}", accumulator.unwrap());
}

/// 640
fn task2() {
    let instruction_set: InstructionSet =
        std::fs::read_to_string("input").unwrap().parse().unwrap();

    let accumulator = (0..instruction_set.0.len())
        .map(|i| {
            let instruction = instruction_set.0[i];
            let instruction = match instruction.operation {
                Operation::NoOperation(arg) => Instruction {
                    operation: Operation::Jump(arg),
                    ..instruction
                },
                Operation::Accumulator(_) => instruction,
                Operation::Jump(arg) => Instruction {
                    operation: Operation::NoOperation(arg),
                    ..instruction
                },
            };

            let mut cloned = instruction_set.clone();
            cloned.0[i] = instruction;
            cloned
        })
        .find_map(
            |set| match Computer::execute_instructions_until_repeated(set) {
                Ok(_) => None,         // found repeated instruction
                Err(acc) => Some(acc), // ended with result
            },
        );

    println!("Task 2 answer: {}", accumulator.unwrap());
}

struct Computer {
    cursor: i32,
    accumulator: i32,
    instruction_set: InstructionSet,
    executed_instruction: HashSet<Instruction>,
}

#[derive(Debug, Clone)]
struct InstructionSet(Vec<Instruction>);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Instruction {
    index: usize,
    operation: Operation,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Operation {
    NoOperation(i32),
    Accumulator(i32),
    Jump(i32),
}

impl Computer {
    fn execute_instructions_until_repeated(instructions: InstructionSet) -> Result<i32, i32> {
        let mut computer = Self {
            cursor: 0,
            accumulator: 0,
            instruction_set: instructions,
            executed_instruction: HashSet::new(),
        };

        while computer.execute_next_instruction()? {}

        Ok(computer.accumulator)
    }

    fn execute_next_instruction(&mut self) -> Result<bool, i32> {
        let next_instruction = self.instruction_set.0.get(self.cursor as usize);

        return if let Some(&next_instruction) = next_instruction {
            Ok(if self.executed_instruction.insert(next_instruction) {
                match next_instruction.operation {
                    Operation::NoOperation(_) => {
                        self.cursor += 1;
                    }
                    Operation::Accumulator(arg) => {
                        self.accumulator += arg;
                        self.cursor += 1;
                    }
                    Operation::Jump(arg) => self.cursor += arg,
                }
                true
            } else {
                false
            })
        } else {
            Err(self.accumulator)
        };
    }
}

impl FromStr for InstructionSet {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let r: Result<Vec<Instruction>, Self::Err> = s
            .lines()
            .map(|line| line.parse::<Operation>())
            .enumerate()
            .map(|(index, r)| r.map(|operation| Instruction { index, operation }))
            .collect();

        Ok(InstructionSet(r?))
    }
}

impl FromStr for Operation {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(' ');
        let op = split.next().ok_or(format!("Illegal format {}", s))?;
        let arg = split
            .next()
            .ok_or(format!("Illegal format {}", s))?
            .parse()?;

        Ok(match op {
            "nop" => Operation::NoOperation(arg),
            "acc" => Operation::Accumulator(arg),
            "jmp" => Operation::Jump(arg),
            _ => panic!("Unknown command {}", op),
        })
    }
}

#[cfg(test)]
mod tests {}
