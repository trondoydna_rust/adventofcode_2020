use std::str::FromStr;

fn main() {
    task1();
    task2();
}

/// 447
fn task1() {
    let input: VecPolicyPassword = std::fs::read_to_string("input")
        .unwrap()
        .parse()
        .unwrap();
    let valid_count = input.0.iter().filter(|pp| pp.is_valid_task1()).count();
    println!("Task 1 answer: {}", valid_count);
}

/// 249
fn task2() {
    let input: VecPolicyPassword = std::fs::read_to_string("input")
        .unwrap()
        .parse()
        .unwrap();
    let valid_count = input.0.iter().filter(|pp| pp.is_valid_task2()).count();
    println!("Task 1 answer: {}", valid_count);
}

struct VecPolicyPassword(Vec<PolicyPassword>);

struct PolicyPassword {
    policy: Policy,
    password: String,
}

struct Policy {
    num1: usize,
    num2: usize,
    letter: String,
}

impl PolicyPassword {
    fn is_valid_task1(&self) -> bool {
        let letter_count = self.password.matches(self.policy.letter.as_str()).count();
        return letter_count >= self.policy.num1 && letter_count <= self.policy.num2;
    }

    fn is_valid_task2(&self) -> bool {
        let index1 = self.policy.num1 - 1;
        let index2 = self.policy.num2 - 1;
        let first_valid = &self.password[index1..(index1+1)] == self.policy.letter.as_str();
        let second_valid = &self.password[index2..(index2+1)] == self.policy.letter.as_str();

        first_valid ^ second_valid
    }
}

impl FromStr for VecPolicyPassword {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let res: Result<Vec<PolicyPassword>, Box<dyn std::error::Error>> = s
            .lines()
            .map(|line| line.parse::<PolicyPassword>())
            .collect();

        Ok(VecPolicyPassword(res?))
    }
}

impl FromStr for PolicyPassword {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut iter = s.split(':');
        let policy = iter
            .next()
            .ok_or_else(|| format!("No : between policy and password found in: {}", s))?;
        let policy = policy.parse()?;
        let password = iter
            .next()
            .ok_or_else(|| format!("No : between policy and password found in: {}", s))?
            .trim()
            .to_owned();

        Ok(PolicyPassword { policy, password })
    }
}

impl FromStr for Policy {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut iter = s.split_whitespace();
        let numbers = iter
            .next()
            .ok_or_else(|| format!("No space between numbers and letter found in: {}", s))?;
        let letter = iter
            .next()
            .ok_or_else(|| format!("No space between numbers and letter found in: {}", s))?
            .to_owned();

        let mut iter = numbers.split('-');
        let min = iter
            .next()
            .ok_or_else(|| format!("No - between numbers in: {}", s))?;
        let min = min.parse()?;
        let max = iter
            .next()
            .ok_or_else(|| format!("No - between numbers in: {}", s))?;
        let max = max.parse()?;

        Ok(Policy { num1: min, num2: max, letter })
    }
}

#[cfg(test)]
mod tests {
    use crate::VecPolicyPassword;

    #[test]
    fn test_task1() {
        let input = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc";
        let input: VecPolicyPassword = input.parse().unwrap();
        let input = input.0;

        assert!(input[0].is_valid_task1());
        assert!(!input[1].is_valid_task1());
        assert!(input[2].is_valid_task1());
    }

    #[test]
    fn test_task2() {
        let input = "1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc";
        let input: VecPolicyPassword = input.parse().unwrap();
        let input = input.0;

        assert!(input[0].is_valid_task2());
        assert!(!input[1].is_valid_task2());
        assert!(!input[2].is_valid_task2());
    }
}