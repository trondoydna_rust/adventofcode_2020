use std::str::FromStr;

fn main() {
    task1();
    task2();
}

/// 228
fn task1() {
    let input: Map = std::fs::read_to_string("input").unwrap().parse().unwrap();
    println!("Task 1 answer: {}", input.count_trees_encountered(3, 1));
}

/// 6818112000
fn task2() {
    let input: Map = std::fs::read_to_string("input").unwrap().parse().unwrap();

    let sum = input.count_trees_encountered(1, 1)
        * input.count_trees_encountered(3, 1)
        * input.count_trees_encountered(5, 1)
        * input.count_trees_encountered(7, 1)
        * input.count_trees_encountered(1, 2);
    println!("Task 2 answer: {}", sum);
}

#[derive(Debug)]
struct Map {
    coordinates: Vec<Vec<Coordinate>>,
}

#[derive(Debug, Clone, Copy)]
enum Coordinate {
    OPEN,
    TREE,
}

impl Map {
    fn count_trees_encountered(&self, right: usize, down: usize) -> usize {
        let row_len = self.coordinates[0].len();
        (0..self.coordinates.len())
            .filter(|row| row % down == 0)
            .enumerate()
            .map(|(i, row)| (row, (i * right) % row_len))
            .map(|(row, column)| self.coordinates[row][column].clone())
            .filter(|c| match c {
                Coordinate::OPEN => false,
                Coordinate::TREE => true,
            })
            .count()
    }
}

impl FromStr for Map {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Map {
            coordinates: s
                .lines()
                .map(|line| {
                    line.chars()
                        .map(|c| match c {
                            '.' => Coordinate::OPEN,
                            '#' => Coordinate::TREE,
                            _ => panic!("Unknown input char {}", c),
                        })
                        .collect::<Vec<Coordinate>>()
                })
                .collect(),
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::Map;

    const INPUT: &str = r#"
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
            "#;

    #[test]
    fn test_task1() {
        let map: Map = INPUT.trim().parse().unwrap();

        assert_eq!(7, map.count_trees_encountered(3, 1));
    }

    #[test]
    fn test_task2() {
        let map: Map = INPUT.trim().parse().unwrap();

        assert_eq!(2, map.count_trees_encountered(1, 1));
        assert_eq!(7, map.count_trees_encountered(3, 1));
        assert_eq!(3, map.count_trees_encountered(5, 1));
        assert_eq!(4, map.count_trees_encountered(7, 1));
        assert_eq!(2, map.count_trees_encountered(1, 2));
    }
}
