use std::{str::FromStr, collections::HashSet};

fn main() {
    task1();
    task2();
}

/// 6297
fn task1() {
    let input: VecAnswers = std::fs::read_to_string("input").unwrap().parse().unwrap();

    println!("Task 1 answer: {}", input.calc_sum_answers_task1());
}

/// 3158
fn task2() {
    let input: VecAnswers = std::fs::read_to_string("input").unwrap().parse().unwrap();
    
    println!("Task 2 answer: {}", input.calc_sum_answers_task2());
}

struct VecAnswers(Vec<String>);

impl VecAnswers {

    fn calc_sum_answers_task1(&self) -> usize {
        self.0
            .iter()
            .map(|group| group.replace("\n", ""))
            .map(|group| group
                    .chars()
                    .fold(
                        HashSet::new(), 
                        |mut distinct, next| {
                            distinct.insert(next);
                            distinct
                        }
                    )                    
                    .len()
            )
            .sum()
    }

    fn calc_sum_answers_task2(&self) -> usize {
        self.0
            .iter()
            .map(|group| {
                group
                    .lines()
                    .fold(
                        HashSet::new(), 
                        |mut all, cur_line| {
                            cur_line
                                .chars()
                                .for_each(|c|
                                    if group.lines().all(|group_line| group_line.contains(c)) {
                                        all.insert(c);
                                    }
                                );
                            all
                        }
                    )
                    .len()
                } 
            )
            .sum()
    }

}

impl FromStr for VecAnswers {
    type Err = Box<dyn std::error::Error>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(VecAnswers(s
            .split("\n\n")
            .map(|group| group.to_owned())
            .collect()
        ))
    }
}
